import { CUSTOM_ELEMENTS_SCHEMA, Component } from '@angular/core';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss'
})
export default class HomeComponent {
  emailAddress = "sepehrazizi13@gmail.com";
  showEmail = false;

  phoneNumber = "+98 912 67 27 001";
  showPhoneNumber = false;

  get email(): string {
    if (this.showEmail) {
      return this.emailAddress;
    } else {
      return Array(this.emailAddress.length).fill('\u{2217}').join('');
    }
  }

  get phone(): string {
    if (this.showPhoneNumber) {
      return this.phoneNumber;
    } else {
      return Array((this.phoneNumber.replaceAll(' ', '')).length).fill('\u{2217}').join('');
    }
  }

  openEmail(): void {
    window.open(`mailto:${this.emailAddress}`, '_blank');
  }

  openPhoneNumber(): void {
    window.open(`tel:${this.phoneNumber.replaceAll(' ', '')}`, '_blank');
  }
}
