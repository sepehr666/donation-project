FROM node:latest AS builder

WORKDIR /app
COPY package.json .
RUN npm i
COPY . .
RUN npm run build


FROM nginx:alpine
COPY --from=builder /app/dist/donation/browser/ /usr/share/nginx/html/
EXPOSE 80